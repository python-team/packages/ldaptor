from twisted.python import components

class ICurrentDN(components.Interface):
    """Marker interface for current DN for Ldaptor-webui."""
